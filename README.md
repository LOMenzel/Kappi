# Kappi

> Author: Leonard-Orlando Menzel

> Datum: 19.12.2021

<hr>

## Über das Projekt
Die Website stellt eine Lernplatform da, deren Algorhitmus auf dem von [Anki](https://github.com/ankitects/anki) beruht, jedoch vollständig kollaborativ genutzt werden kann. Es handelt sich um ein spaced repetition & active recall Prinzip.
Da es sich um ein Schulprojekt handelt erfolgt die Dokumentation über ein [Pflichtenheft](https://docs.google.com/document/d/1iFDPQ7m8bli4c9Cg_8fpXniyE0Hk6XLpDd1PPH6XAUs/edit?usp=sharing)

### Das Projekt ist durch folgende Software möglich:
    Angular,
    Anki,
    NestJS,
    MongoDB,
    TypeORM,
    Angular Material