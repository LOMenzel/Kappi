export interface Settings{
    startLeichtigkeit:number;
    startEinfach:number;
    lernenSchritte:number[];
    startGut:number;
    erneutLernenSchritte:number[];
    bonus:number;
    faktorNachErneutemLernen:number;
    neueProTag:number;
    kartenProTag:number;
    minIntervall:number;
    maxIntervall:number;
}